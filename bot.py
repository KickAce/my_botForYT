﻿import time, random
from threading import Thread
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium import webdriver

def loadAgents():
    file = './agents.txt'
    uas = []
    with open(file, 'r') as uaf:
        for ua in uaf.readlines():
            if ua:
                uas.append(ua.strip())
    return uas

def randomEvent(driver):
    RUN = True
    while RUN == True:
        time.sleep(40)
        try:
            g = random.randint(0 , 25)
            if g < 3:
                print('Scrolling...')
                y = 0

                while y < random.randint(600, 2000):
                    driver.execute_script("window.scrollTo(0, " + str(y) + ")")
                    y += 10
                time.sleep(random.randint(6, 30))

                while y >= 0:
                    driver.execute_script("window.scrollTo(0, " + str(y) + ")")
                    y -= 10

            if g > 22:
                print('Click on the size button')
                driver.find_element_by_class_name('ytp-size-button').click()
        except Exception as ex:
            RUN = False

def clickToAD(driver):
    RUN = True
    while RUN == True:
        time.sleep(5)
        try:
            if driver.find_element_by_class_name('videoAdUiVisitAdvertiserLink') != '':
                time.sleep(15)
                driver.find_element_by_class_name('videoAdUiVisitAdvertiserLink').click()  # Click to ad
                print('Click ad')
                driver.find_element_by_class_name('ytp-play-button').click()  # Play
                print('Click play')
                time.sleep(25)
                driver.find_element_by_class_name('videoAdUiSkipContainer').click()  # Click to Пропустить
                print('Click Skip')
                RUN = False
        except Exception as ex:
            ':except'

        try:
            if driver.find_element_by_class_name('text-title') != '':
                time.sleep(5)
                driver.find_element_by_class_name('text-title').click()  # Click to ad
                print('Click ad')
                driver.find_element_by_class_name('ytp-play-button').click()  # Play
                print('Click play')
                RUN = False
        except Exception as ex:
            ':except'

        try:
            if driver.find_element_by_class_name('image-container') != '':
                time.sleep(5)
                driver.find_element_by_class_name('image-container').click()  # Bar
                print('Click bar')
                driver.find_element_by_class_name('ytp-play-button').click()  # Play
                print('Click play')
                RUN = False


        except Exception as ex:
            ':except'

sizewind = [[1920, 1080], [1024, 768], [1280, 1024], [1080, 720], [800, 600], [1366, 768], [1680, 768], [1440, 900]]


def oneViewer(proxy, agent, ads, link, g):

    try:
        print(proxy[0] + ':' + proxy[1])
        profile = webdriver.FirefoxProfile()
        profile.set_preference('network.proxy.ssl_port', int(proxy[1]))
        profile.set_preference('network.proxy.ssl', str(proxy[0]))
        profile.set_preference('network.proxy.http_port', int(proxy[1]))
        profile.set_preference('network.proxy.http', str(proxy[0]))
        profile.set_preference('network.proxy.type', 1)
        profile.set_preference( "places.history.enabled", False )
        profile.set_preference( "privacy.clearOnShutdown.offlineApps", True )
        profile.set_preference( "privacy.clearOnShutdown.passwords", True )
        profile.set_preference( "privacy.clearOnShutdown.siteSettings", True )
        profile.set_preference( "privacy.sanitize.sanitizeOnShutdown", True )
        profile.set_preference( "signon.rememberSignons", False )
        profile.set_preference( "network.cookie.lifetimePolicy", 2 )
        profile.set_preference( "network.dns.disablePrefetch", True )
        profile.set_preference( "network.http.sendRefererHeader", 0 )
        profile.set_preference( "javascript.enabled", True )
        profile.set_preference('general.useragent.override', agent)

        binary = FirefoxBinary('E:/Programs/Firefox/firefox.exe')

        driver = webdriver.Firefox(firefox_profile=profile, firefox_binary=binary)
        # driver.set_window_position(5000, 0)
        driver.set_page_load_timeout(70)
        size = random.choice(sizewind)
        driver.set_window_size(size[0], size[1])
        driver.get(link)
        if ads == 'y' and g %4 == 0 and g != 0:
            print('Клик по рекламе включен!')
            t = Thread(target=clickToAD, args=(driver,))
            t.daemon = True
            t.start()

        event = Thread(target=randomEvent, args=(driver,))
        event.daemon = True
        event.start()


        time.sleep(random.randint(300, 500))
        driver.quit()

    except Exception as ex:
        driver.quit()

def getProxies():
    proxies = []
    f = open('./proxy.txt', 'r')
    for row in f.readlines():
        proxies.append(row.replace('\n', '').split(':'))
    f.close()
    return proxies

agents = loadAgents()
ads = 'y'
link = 'https://www.youtube.com/watch?v=Ln6YHFc8KTk'
proxies = getProxies()
t = []
countFlows = 7
# int(input('Сколько Потоков?'))
count = input('Сколько раз?')

if count == 'max':
    g = 0
    while proxies != []:
        for i in range(countFlows):
            proxy = random.choice(proxies)
            proxies.remove(proxy)
            t.append(Thread(target=oneViewer, args=(proxy, random.choice(agents), ads, link, g)))

        for v in t:
            v.start()
        for v in t:
            v.join()
        t = []
        g += 1

else:

    for g in range(int(count)):
        print(g)
        if proxies != []:
            for i in range(countFlows):
                proxy = random.choice(proxies)
                proxies.remove(proxy)
                t.append(Thread(target=oneViewer, args=(proxy, random.choice(agents), ads, link, g)))

            for v in t:
                v.start()
            for v in t:
                v.join()
            t = []
        else:
            break



print('---End---')




