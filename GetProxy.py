import requests
from bs4 import BeautifulSoup


def getProxyFromSite():
    page = ''
    proxy = []
    i = 0
    while i != 10:

        responce = requests.get('http://hideme.name/ru/proxy-list/?start=' + str(page))
        soup = BeautifulSoup(responce.content, "html.parser")
        rows = soup.findAll("tr")
        for row in rows:
            if len(row) == 7 and row.contents[5].contents[0] == 'Высокая' and row.contents[4].contents[0] == 'HTTPS':
                print('Get: ' + str(row.contents[0].contents[0])+ ':' + str(row.contents[1].contents[0]))
                proxy.append(str(row.contents[0].contents[0])+ ':' + str(row.contents[1].contents[0]))

        if page == '':
            page = 64
        else:
            page += 64
        i += 1
    return proxy


def getProxyFromFile():
    rows = []
    f = open('./proxy.txt', 'r')
    for row in f.readlines():
        rows.append(row.replace('\n', ''))
    f.close()
    return rows

def checkAndAddProxies():
    new_proxy = list(set(getProxyFromSite()) - set(getProxyFromFile()))
    print('Add proxies: ' + str(new_proxy))

    file = open('./proxy.txt', 'a')
    for row in new_proxy:
        file.write(row + "\n")
    file.close()

checkAndAddProxies()



# responce = requests.get('https://www.sslproxies.org/')
# soup = BeautifulSoup(responce.content, "html.parser")
# rows = soup.findAll("tr")
# proxy = []
# for row in rows:
#     if len(row) == 8 and row.contents[4].contents[0] == 'elite proxy':
#         print('Get: ' + str(row.contents[0].contents[0]) + ':' + str(row.contents[1].contents[0]))
#         proxy.append(str(row.contents[0].contents[0]) + ':' + str(row.contents[1].contents[0]))